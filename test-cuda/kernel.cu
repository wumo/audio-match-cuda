#include "cudaCalc.h"
#include "audio.h"
#include <time.h>
#include <stdlib.h>
#include <chrono>
#include <sstream>
#include <regex>
#include <Windows.h>
#include <iomanip>

std::string properTime(int duration_mili) {
	std::ostringstream sout;
	if (duration_mili < 1000)
		sout << duration_mili << "ms";
	else if (duration_mili < 60 * 1000)
		sout << duration_mili / 1000 << "s" << duration_mili % 1000 << "ms";
	else if (duration_mili < 60 * 60 * 1000)
		sout << duration_mili / (60 * 1000) << "min" << properTime(duration_mili % (60 * 1000));
	else if (duration_mili < 24 * 60 * 60 * 1000)
		sout << duration_mili / (60 * 60 * 1000) << "h" << properTime(duration_mili % (60 * 60 * 1000));
	else
		sout << duration_mili / (24 * 60 * 60 * 1000) << "day" << properTime(duration_mili % (24 * 60 * 60 * 1000));

	return sout.str();
}

void init() {
	av_register_all();
	processError(cudaSetDevice(0));
}

typedef struct {
	const char* name;
	std::vector<float>* vector_ptr;
	float* dev_ptr;
} buzz_struct;

auto skip = 1000;
auto threshold = 0.9;

std::vector<buzz_struct>* loadBuzz(std::vector<const char*>& buzzfiles) {
	auto buzz_struct_s = new std::vector<buzz_struct>();
	for (auto& buzzfile : buzzfiles) {
		auto buzz = readAll(buzzfile);
		auto length = 0.0;
		for (auto i = 0; i < buzz->size(); i += skip) {
			auto w = buzz->at(i);
			length += w * w;
		}
		length = sqrtf(length);
		for (auto i = 0; i < buzz->size(); i += skip)
			buzz->at(i) /= length;
		float* dev_buzz = nullptr;
		processError(cudaMalloc(reinterpret_cast<void**>(&dev_buzz), buzz->size() * sizeof(float)));
		processError(cudaMemcpy(dev_buzz, buzz->data(), buzz->size() * sizeof(float), cudaMemcpyHostToDevice));

		buzz_struct_s->push_back(buzz_struct{buzzfile, buzz,dev_buzz});
	}
	return buzz_struct_s;
}

void freeBuzz(std::vector<buzz_struct>* buzzPtrs) {
	//free buzz
	for (auto& buzz_ptr : *buzzPtrs) {
		delete buzz_ptr.vector_ptr;
		cudaFree(buzz_ptr.dev_ptr);
	}
	delete buzzPtrs;
}

void match_and_split(const char* testfilename, const char* outputfilename, std::vector<buzz_struct>& buzzes) {
	std::cout << ">" << testfilename;
	auto sample_rate = 0;
	auto test = readAll(testfilename, &sample_rate);
	std::cout << ">:" << std::endl;
	auto test_ptr = test->data();

	float* dev_test = nullptr;
	result* dev_block_result = nullptr;
	processError(cudaMalloc(reinterpret_cast<void**>(&dev_test), test->size() * sizeof(float)));
	processError(cudaMalloc(reinterpret_cast<void**>(&dev_block_result), max_block_num * sizeof(result)));
	processError(cudaMemcpy(dev_test, test_ptr, test->size() * sizeof(float), cudaMemcpyHostToDevice));
	std::vector<std::pair<int, int>> best_results;
	for (auto& buzz : buzzes) {
		auto start = std::chrono::system_clock::now();
		std::vector<result> largetThanThreshold;
		auto best = worst;
		auto buzz_size = buzz.vector_ptr->size();
		auto max_batch_task = max_block_num * max_threads_per_block;
		auto taskIdx_offset = 0;
		auto total_task = test->size() - buzz_size + 1;
		int remaining_task = test->size() - buzz_size + 1;
		while (remaining_task > 0) {
			auto batch_task = min(remaining_task, max_batch_task);
			auto batch_size = batch_task + buzz_size - 1;

			auto result = cuda_compute(buzz.dev_ptr, buzz_size, dev_test + taskIdx_offset, batch_size, dev_block_result, skip);
			result.index += taskIdx_offset;

			if (threshold > 0)
				if (result.similarity > threshold)
					largetThanThreshold.push_back(result);
			best = bestOf(result, best);

			taskIdx_offset += batch_task;
			remaining_task -= batch_task;
		}
		auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count();
		std::cout <<"\t"<<buzz.name<<  ": time=" << properTime(elapsed) << "  best=" << properTime(1000.0 * best.index / sample_rate) << ":" << best.similarity;
		if (threshold > 0) {
			std::cout << "  threshold(" << threshold << ")=";
			for (auto& good : largetThanThreshold) {
				std::cout << properTime(1000.0 * good.index / sample_rate) << ":" << good.similarity << ", ";
			}
		}
		std::cout << std::endl;

		if (threshold > 0) {
			for (auto& entry:largetThanThreshold)
				best_results.push_back(std::make_pair(entry.index, entry.index + buzz_size));
		} else
			best_results.push_back(std::make_pair(best.index, best.index + buzz_size));
	}

	std::cout
		<<
		" \t=> "
		<<
		outputfilename
		<<
		std::endl;
	delete
		test;
	cudaFree(dev_test);
	cudaFree(dev_block_result);
	if
	(best_results
		.
		empty()
	)
		return;
	split(testfilename, [&best_results](auto start, auto end) {
		      auto include = true;
		      for (auto& condition : best_results) {
			      if (condition.first <= start && end <= condition.second) {
				      include = false;
				      break;
			      }
		      }
		      return include;
	      }, outputfilename);
}

static std::regex instant("^(?:([0-9]{1,2}):){0,2}([0-9]{1,2}(?:\\.[0-9]{1,3})?)$");

static double str2s(char* str) {
	std::cmatch match;
	if (!std::regex_match(str, match, instant)) {
		fprintf(stderr, "error time format!\n");
		exit(1);
	}
	auto total_s = 0.0;
	auto unit = 1;
	for (auto i = match.size() - 1; i > 0; --i) {
		auto ret = atof(match[i].str().c_str());
		total_s += ret * unit;
		unit *= 60;
	}
	return total_s;
}

void error_promt(const char* program_name) {
	exit(1);
}

int main(int argc, char** argv) {
	auto i = 1;
	std::string audio_suffix = "m4a";
	std::string output_prefix = "_";
	std::string buzz_prefix = "buzz";
	while (i < argc) {
		auto cmd = argv[i++][1];
		switch (cmd) {
			case 'M':
				audio_suffix = * new std::string(argv[i ++]);
				break;
			case 'O':
				output_prefix = * new std::string(argv[i++]);
				break;
			case 'B':
				buzz_prefix = * new std::string(argv[i++]);
				break;
			case 'S':
				skip = atoi(argv[i++]);
				break;
			case 'T':
				threshold = atof(argv[i++]);
				break;
			default:
				error_promt(argv[0]);
		}
	}
	std::cout << "skip=" << skip << std::endl;
	std::cout << "threshold=" << threshold << std::endl;
	std::vector<const char*> buzzfiles, testfiles; {
		HANDLE hFind;
		WIN32_FIND_DATA data;
		hFind = FindFirstFile(std::string("./*.").append(audio_suffix).c_str(), &data);
		if (hFind != INVALID_HANDLE_VALUE) {
			do {
				auto tmp = new char[strlen(data.cFileName) + 1];
				strcpy(tmp, data.cFileName);

				if (0 == std::string(tmp).compare(0, buzz_prefix.length(), buzz_prefix)) { //buzz file
					std::cout << "buzz file: " << tmp << std::endl;
					buzzfiles.push_back(tmp);
				} else
					testfiles.push_back(tmp);
			} while (FindNextFile(hFind, &data));
			FindClose(hFind);
		}
	}

	if (!buzzfiles.empty() && !testfiles.empty()) {
		init();

		auto buzzPtrs = loadBuzz(buzzfiles);

		for (auto& testfile : testfiles) {
			auto tmp = std::string(output_prefix);
			match_and_split(testfile, tmp.append(testfile).c_str(), *buzzPtrs);
		}

		freeBuzz(buzzPtrs);
		processError(cudaDeviceReset());

		for (auto& buzzfile : buzzfiles)
			delete[] buzzfile;
		for (auto& testfile : testfiles)
			delete[] testfile;
	}

	system("pause");
	return 0;

}
