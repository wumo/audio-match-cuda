#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <algorithm>
#include <iostream>

typedef struct {
	float similarity;
	int index;
} result;

#define worst result{-INFINITY,-1}

__device__ result match(float* test, int offset, float* buzz, int& buzz_size, int& skip) {
	result best{0,offset};
	float length = 0;
	for (auto i = 0; i < buzz_size; i += skip) {
		auto delta = buzz[i] * test[offset + i];
		length += test[offset + i] * test[offset + i];
		best.similarity += delta;
	}
	best.similarity /= sqrtf(length);
	return best;
}

inline __device__ __host__ result bestOf(result& a, result& b) {
	return (a.similarity > b.similarity) ? a : b;
}

typedef struct {
	result* block_result;
	float* buzz;
	int buzz_size;
	float* test;
	int test_size;
	int task_num_per_threads;
	int extra_task_bound;
	int threads_per_block;
	int skip;
} config;


__global__ void CrossCorrelationKernel(config config) {
	auto globalIdx = blockIdx.x * config.threads_per_block + threadIdx.x;
	int task_startIdx, task_endIdx;
	task_startIdx = config.task_num_per_threads * globalIdx;

	//thread finishs its own task first.
	if (globalIdx < config.extra_task_bound) {
		task_startIdx += globalIdx;//inclusive
		task_endIdx = task_startIdx + config.task_num_per_threads + 1;
	} else {
		task_startIdx += config.extra_task_bound;
		task_endIdx = task_startIdx + config.task_num_per_threads;
	}

	if (task_startIdx > config.test_size)
		return;

	auto best = worst;
	for (auto taskIdx = task_startIdx; taskIdx < task_endIdx; ++taskIdx) {
		auto ret = match(config.test, taskIdx, config.buzz, config.buzz_size, config.skip);
		best = bestOf(ret, best);
	}
	extern __shared__ result block_best_result[];
	block_best_result[threadIdx.x] = best;

	__syncthreads();
	//reduce to the best result in a block
	auto gate = 1;
	int idx = threadIdx.x;
	while (gate < config.threads_per_block) {
		if (idx % gate == 0 && idx + gate < config.threads_per_block)
			best = bestOf(block_best_result[idx], block_best_result[idx + gate]);
		__syncthreads();
		if (idx % gate == 0 && idx + gate < config.threads_per_block)
			block_best_result[idx] = best;
		gate <<= 1;
	}
	if (idx == 0)
		config.block_result[blockIdx.x] = best;
}

#define max_block_num 40
#define max_threads_per_block 1024

void processError(cudaError_t cudaStatus) {
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, " error %s!\n", cudaGetErrorString(cudaStatus));
		system("pause");
		exit(EXIT_FAILURE);
	}
}

result cuda_compute(float* dev_buzz, int buzz_size, float* dev_test, int test_size, result* dev_block_result, int skip) {
	auto best = worst;
	auto total_task = test_size - buzz_size + 1;
	if (total_task <= 0)
		return best;
	auto threads_per_block = std::min(max_threads_per_block, total_task);
	auto block_num = std::min(max_block_num, total_task / threads_per_block);
	auto total_threads = block_num * threads_per_block;
	auto task_num_per_threads = total_task / total_threads;
	auto extra_task_bound = total_task % total_threads;
	config config = {
		dev_block_result,
		dev_buzz,
		buzz_size,
		dev_test,
		test_size,
		task_num_per_threads,
		extra_task_bound,
		threads_per_block,
		skip,
	};

	CrossCorrelationKernel <<<block_num, threads_per_block, threads_per_block * sizeof(result) >>>(config);

	processError(cudaGetLastError());
	processError(cudaDeviceSynchronize());

	auto block_result = new result[block_num];
	processError(cudaMemcpy(block_result, dev_block_result, block_num * sizeof(result), cudaMemcpyDeviceToHost));

	for (auto i = 0; i < block_num; i++)
		best = bestOf(best, block_result[i]);

	delete[] block_result;
	return best;
}
