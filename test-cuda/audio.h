#pragma once
#include <algorithm>
#include <vector>
#include <iostream>

extern "C" {
#include "libavformat/avformat.h"
}
#pragma comment(lib, "avcodec.lib")
#pragma comment(lib, "avformat.lib")
#pragma comment(lib, "avdevice.lib")
#pragma comment(lib, "avfilter.lib")
#pragma comment(lib, "avutil.lib")
#pragma comment(lib, "postproc.lib")
#pragma comment(lib, "swresample.lib")
#pragma comment(lib, "swscale.lib")

template <typename P>
static void format_general(uint8_t* data, int nb_samples, std::vector<float>& buzz) {
	auto channel = reinterpret_cast<P*>(data);
	for (auto i = 0; i < nb_samples; ++i) {
		auto sample = static_cast<float>(channel[i]);
		buzz.push_back(sample);
	}
}

std::vector<float>* readAll(const char* filename, int* sample_rate = nullptr) {
	auto input_format_context = avformat_alloc_context();
	avformat_open_input(&input_format_context, filename, nullptr, nullptr);//Open the input file to read from it.
	avformat_find_stream_info(input_format_context, nullptr);//Get information on the input file (number of streams etc.).
	//	av_dump_format(input_format_context, 0, filename, 0);//Dump valid information onto standard error
	auto stream = *input_format_context->streams[0];
	if (sample_rate != nullptr)
		*sample_rate = stream.codecpar->sample_rate;
	auto format = static_cast<AVSampleFormat>(stream.codecpar->format);
	void (*format_func)(uint8_t*, int, std::vector<float>&) = nullptr;
	switch (format) {
		case AV_SAMPLE_FMT_S16P:
			format_func = &format_general<int16_t>;
			break;
		case AV_SAMPLE_FMT_S32P:
			format_func = &format_general<int32_t>;
			break;
		case AV_SAMPLE_FMT_FLTP:
			format_func = &format_general<float>;
			break;
		case AV_SAMPLE_FMT_DBLP:
			format_func = &format_general<double>;
			break;
		default:
			std::cout << "unsupported format" << av_get_sample_fmt_name(format) << std::endl;
			exit(1);
			break;
	}
	auto buzz = new std::vector<float>();
	auto size = static_cast<int>(stream.duration * 1.0 * stream.time_base.num / stream.time_base.den * stream.codecpar->sample_rate);
	buzz->reserve(size);
	auto input_codec = avcodec_find_decoder(input_format_context->streams[0]->codecpar->codec_id);//Find a decoder for the audio stream.
	auto input_codec_context = avcodec_alloc_context3(input_codec);//allocate a new decoding context
	avcodec_parameters_to_context(input_codec_context, input_format_context->streams[0]->codecpar);//initialize the stream parameters with demuxer information
	avcodec_open2(input_codec_context, input_codec, nullptr);
	auto frame = av_frame_alloc();
	AVPacket packet;
	av_init_packet(&packet);
	while (av_read_frame(input_format_context, &packet) >= 0) {
		auto error = avcodec_send_packet(input_codec_context, &packet);
		av_packet_unref(&packet);
		//In particular, we don't expect AVERROR(EAGAIN), 'cause all frames will be read out. so it won't happen.
		if (!error)
			while (!avcodec_receive_frame(input_codec_context, frame)) //decode all of the frames.
				format_func(frame->data[0], frame->nb_samples, *buzz);
	}
	//cleanup
	if (frame != nullptr) av_frame_free(&frame);
	if (input_codec_context != nullptr)avcodec_free_context(&input_codec_context);
	if (input_format_context != nullptr)avformat_close_input(&input_format_context);
	return buzz;
}

template <typename F>
void split(const char* intput_filename, F& condition_inclusive, const char* output_filename) {
	//input file
	auto input_format_context = avformat_alloc_context();
	avformat_open_input(&input_format_context, intput_filename, nullptr, nullptr);//Open the input file to read from it.
	avformat_find_stream_info(input_format_context, nullptr);//Get information on the input file (number of streams etc.).

	//output file
	AVFormatContext* output_format_context = nullptr;
	avformat_alloc_output_context2(&output_format_context, nullptr, nullptr, output_filename);
	avio_open(&output_format_context->pb, output_filename, AVIO_FLAG_WRITE);
	for (auto i = 0; i < input_format_context->nb_streams; ++i) {
		auto output_stream = avformat_new_stream(output_format_context, nullptr);//Create a new audio stream in the output file container.
		avcodec_parameters_copy(output_stream->codecpar, input_format_context->streams[i]->codecpar);
	}
	//Some container formats (like MP4) require global headers to be present Mark the encoder so that it behaves accordingly.
	//    if (output_format_context->oformat->flags & AVFMT_GLOBALHEADER)
	//        output_codec_context->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
	avformat_write_header(output_format_context, nullptr); //init muxer, write output file header

	AVPacket packet;
	av_init_packet(&packet);
	auto pts = 0;
	auto duration = 0;
	auto current_pts = 0;
	while (true) {
		auto ret = av_read_frame(input_format_context, &packet);
		duration = packet.duration;
		if (ret < 0)break;
		if (condition_inclusive(pts, pts + duration)) {
			//copy packet
			packet.pts = current_pts;
			packet.dts = current_pts;
			current_pts += duration;
			packet.pos = -1;
			ret = av_interleaved_write_frame(output_format_context, &packet);
			if (ret < 0) {
				fprintf(stderr, "Error muxing packet\n");
				break;
			}
		}
		pts += duration;
		av_packet_unref(&packet);
	}
	av_write_trailer(output_format_context);//Write the trailer of the output file container.
	//cleanup
	if (output_format_context) {
		avio_closep(&output_format_context->pb);
		avformat_free_context(output_format_context);
	}
	if (input_format_context) {
		avformat_close_input(&input_format_context);
		avformat_free_context(input_format_context);
	}
}
